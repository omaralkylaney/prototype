﻿using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmPrototype.Core.Models.Generated;
using UmPrototype.Core.Models.Models;

namespace UmPrototype.Core.Controllers
{
	public class BrokerController : RenderMvcController
	{
    public ActionResult Index(ContentModel model, string broker)
    {
			var modelBase = model.Content as BaseModel;
			modelBase.Broker = new Broker { Name = broker };
			var template = Services.FileService.GetTemplate(model.Content.TemplateId.Value);						
			return View(template.VirtualPath,modelBase);
		}
  }
}
