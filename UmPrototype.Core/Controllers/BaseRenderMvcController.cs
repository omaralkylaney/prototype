﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using UmPrototype.Core.Models.Generated;
using UmPrototype.Core.Models.Models;

namespace UmPrototype.Core.Controllers
{
	public class BaseRenderMvcController : RenderMvcController
	{
    /// <summary>
    /// Hijack every request as the Base Model is caching the  
    /// broker object
    /// </summary>    
    public ActionResult Index(ContentModel model,string broker)
    {
      var modelBase = model.Content as BaseModel;
      if(string.IsNullOrEmpty(broker))
			{
        modelBase.Broker = null;
      }
      else
			{
        modelBase.Broker = new Broker { Name = broker };
      }      
      var template = Services.FileService.GetTemplate(model.Content.TemplateId.Value);
      return View(template.VirtualPath, modelBase);      
    }
  }
}
