﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace UmPrototype.Core.RouteHandler
{
	/// <summary>
	/// The generic BrokersRouteHandler page node route handler.
	/// </summary>
	public class BrokersRouteHandler : UmbracoVirtualNodeRouteHandler
	{
		/// <summary>
		/// returns the <see cref="IPublishedContent"/> associated with the route.
		/// </summary>
		/// <param name="requestContext">
		/// The request context.
		/// </param>
		/// <param name="umbracoContext">
		/// The umbraco context.
		/// </param>
		/// <returns>
		/// The <see cref="IPublishedContent"/>.
		/// </returns>
		protected override IPublishedContent FindContent(RequestContext requestContext, UmbracoContext umbracoContext)
		{
			
			var slug = requestContext.RouteData.Values["slug"] as string;
			var root = umbracoContext.Content.GetAtRoot().FirstOrDefault();
			if (string.IsNullOrEmpty(slug))
				return root;
			var page = umbracoContext.Content.GetByRoute($"/{slug}/");
			return page;
			
		}
	}
}
