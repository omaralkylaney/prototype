﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.ModelsBuilder;
using UmPrototype.Core.Models.Generated;
using UmPrototype.Core.Models.Models;

[assembly: ModelsBaseClass(typeof(BaseModel))]
namespace UmPrototype.Core.Models.Generated
{
	public class BaseModel : PublishedContentModel
	{
		public Broker Broker { get; set; }
		public BaseModel(IPublishedContent content) : base(content) {	}
	}
}
