﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Models;
using UmPrototype.Core.Models.Models;

namespace UmPrototype.Core.Models.ViewModels
{
	public class BrokerViewModel : ViewModel
	{		

		public Broker Broker { get; }

    public BrokerViewModel(IPublishedContent content, Broker broker)
      : base(content)
    {
      Broker = broker;      
    }
	}
}
