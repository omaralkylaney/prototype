﻿using Umbraco.Core.Models.PublishedContent;

namespace UmPrototype.Core.Models.ViewModels
{
	public class ViewModel
	{
		public IPublishedContent Content { get; }

		protected ViewModel(IPublishedContent content)
		{
			Content = content;
		}
	}
}