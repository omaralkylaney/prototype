﻿using Umbraco.Core.Composing;
using Umbraco.Web;
using UmPrototype.Core.Controllers;

namespace UmPrototype.Core.Composer
{
	public class SetDefaultRenderMvcControllerComposer: IUserComposer
	{
		public void Compose(Composition composition)
		{
			composition.SetDefaultRenderMvcController(typeof(BaseRenderMvcController));
		}
	}
}
