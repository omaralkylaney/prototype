﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Composing;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using UmPrototype.Core.RouteHandler;

namespace UmPrototype.Core.Composer
{
  public class RegisterCustomRouteComposer : ComponentComposer<RegisterCustomRouteComponent>
  { }

  public class RegisterCustomRouteComponent : IComponent
  {    
    public void Initialize()
    {
      // Custom route to Base Controller which will use a node with a specific broker id 
      // and the slug as IPublishedContent for the current rendering page

      RouteTable.Routes.MapUmbracoRoute("BrokerPage", "brokers/{broker}/{*slug}", new
      {
        controller = "Base",
        action = "Index",
        id = UrlParameter.Optional
      }, new BrokersRouteHandler(), new { slug = @"^(.*)?$" });
    }

    public void Terminate()
    {
      // Nothing to terminate
    }
  }
}
